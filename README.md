# ansible-playbook

ansible-playbook for https://mobilizemuc.org/ using https://framagit.org/klml/ansible-role-mobilizon-release

## Backup

The [roles/postgresqlbackup](playbooks/roles/postgresqlbackup/tasks/main.yaml) dumps a  daily [pg_dumpall](https://www.postgresql.org/docs/10/app-pg-dumpall.html) to `/mnt/backups/backups/`, if this directory exists.


## Dependencies

Install [ansible-role-mobilizon-release](https://framagit.org/klml/ansible-role-mobilizon-release):

```
ansible-galaxy install -r requirements.yaml
```

## Theming

There is a [hackish custom_theme](playbooks/roles/custom_theme/tasks/main.yaml), where you can use [theme/custom.css](theme/custom.css).


## Install

```
ansible-playbook -i inventories/mobilizemuc.org playbooks/install.yaml -e email_password=zzzZZZzzz
```

## testhost

Create testhost at https://console.hetzner.cloud/

* create a security token, https://console.hetzner.cloud/projects/ > security > tokens
* set put that token in your environment as the variable `export HCLOUD_TOKEN=xx`
* install dependency `pip3 install hcloud`
* `ansible-playbook -v playbooks/hcloud-server.yaml`
